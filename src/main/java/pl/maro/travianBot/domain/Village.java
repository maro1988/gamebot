package pl.maro.travianBot.domain;

import lombok.Data;

import java.util.List;

@Data
public class Village {
    private Long id;
    private String createdAt;
    private String updatedAt;
    private int coordinateX;
    private int coordinateY;
    private String name;
    private int population;
    private List<Building> buildings;
}
