package pl.maro.travianBot.domain;

import lombok.Data;

@Data
public class Adventure{
    private int difficulty;
    private int expiredTime;
}
