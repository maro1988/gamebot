package pl.maro.travianBot.domain;

import lombok.Getter;

@Getter
public enum Difficulty {
    HIGH(1),
    LOW(0);

    private int lvl;

    private Difficulty(int lvl){
        this.lvl = lvl;
    }
}
