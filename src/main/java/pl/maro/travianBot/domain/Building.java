package pl.maro.travianBot.domain;

import lombok.Data;

@Data
public class Building {
    private Long id;
    private int level;
    private String type;
    private int field;
}
