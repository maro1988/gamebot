package pl.maro.travianBot.domain;

import lombok.Data;

import java.util.List;

@Data
public class User {

    private Long id;
    private String username;
    private String password;
    private String server;
    private String tribe;
    private String email;
    private String emailPassword;
    private String createdAt;
    private String updatedAt;
    private List<Village> villageList;

    public User(String username, String password, String server) {
        this.username = username;
        this.password = password;
        this.server = server;
    }
}
