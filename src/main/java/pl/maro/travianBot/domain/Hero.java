package pl.maro.travianBot.domain;

import lombok.Data;

@Data
public class Hero {
    private int health = 100;

    public Hero(int health) {
        this.health = health;
    }

    public void goAdventure(Adventure a) {

    }
}
