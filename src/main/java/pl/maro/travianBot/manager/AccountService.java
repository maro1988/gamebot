package pl.maro.travianBot.manager;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pl.maro.travianBot.domain.User;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;
import static pl.maro.travianBot.DataPorcessor.toInt;

public class AccountService {

    public static WebDriver build(User user, WebDriver driver, Task task) {
        login(user, driver);
        while (!task.isDone()) {
            doTask(user, driver, task);
        }
        return driver;
    }

    private static void doTask(User user, WebDriver driver, Task task) {
        int finishLastTaskDate = scanBuildingQueue(driver, user);
        int whenEnough = enoughtResources(driver, task);
        var isDone = checkCurrentLevel(driver, task);
        if (!isDone) {
            try {
                if (finishLastTaskDate > 0 || whenEnough > 0) {
                    var pause = (finishLastTaskDate > whenEnough ? finishLastTaskDate : whenEnough) + 9;
                    System.out.println("uśpiono na " + pause);
                    TimeUnit.SECONDS.sleep(pause);
                    System.out.println("wznowiono");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            var url = task.getBuildingUrl();
            driver.get(url);
            var button = driver.findElement(By.className("section1"))
                    .findElement(By.tagName("button"));
            var tab = button.getAttribute("value").split(" ");
            try {
                var lvl = Integer.valueOf(tab[tab.length - 1]);
                button.click();
                System.out.println("rozpoczęto budowę");
                if (lvl == task.getAimLevel()) {
                    task.setDone(true);
                    driver.get(user.getServer()+"dorf1.php");
                    System.out.println(driver.getCurrentUrl());
                    var finish = scanBuildingQueue(driver, user);
                    System.out.println("ukończono zadanie");
                    System.out.printf("Budowa zostanie ukończona za %s sekund.\n", finish);
                    System.out.printf("uśpiono na %d sekund", finish);
                    TimeUnit.SECONDS.sleep(finish);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            driver.get(user.getServer()+"dorf1.php");
        } else {
            task.setDone(true);
        }
    }

    private static boolean checkCurrentLevel(WebDriver driver, Task task) {
        var currentLevel = toInt(driver.findElement(By.className("level")).getText());
        var aimLevel = task.getAimLevel();
        System.out.printf("currentLevel: %d, aimLevel %d", currentLevel, aimLevel);
        return aimLevel <= currentLevel;
    }

    private static int enoughtResources(WebDriver driver, Task task) {
        driver.get(task.getBuildingUrl());
        System.out.println(driver.getCurrentUrl());
        var stockBar = driver.findElement(By.id("stockBar"));
        var wood = toInt(stockBar.findElement(By.id("stockBarResource1")).getText());
        var clay = toInt(stockBar.findElement(By.id("stockBarResource2")).getText());
        var iron = toInt(stockBar.findElement(By.id("stockBarResource3")).getText());
        var crop = toInt(stockBar.findElement(By.id("stockBarResource4")).getText());
        var s = stockBar.findElement(By.id("stockBarFreeCrop")).getText().trim();
        var cropProduction = toInt(s);

        var stockBarWarehouseWrapper = toInt(stockBar.findElement(By.id("stockBarWarehouseWrapper")).getText());
        var stockBarGranaryWrapper = toInt(stockBar.findElement(By.id("stockBarGranaryWrapper")).getText());

        var contract = driver.findElement(By.id("contract")).findElements(By.tagName("span"));

        var woodRequired = toInt(contract.get(0).getText());
        var clayRequired = toInt(contract.get(1).getText());
        var ironRequired = toInt(contract.get(2).getText());
        var cropRequired = toInt(contract.get(3).getText());
        var cropUsagePerHour = toInt(contract.get(4).getText());

        System.out.printf("pojemność magazynu: %d, pojemność spichlerza: %d\n", stockBarWarehouseWrapper, stockBarGranaryWrapper);
        System.out.printf("drewno: %d, glina: %d, żelazo: %d, zborze: %d, produkcja zborza: %d\n", wood, clay, iron, crop, cropProduction);
        System.out.println("Wymagane:");
        System.out.printf("drewno: %d, glina: %d, żelazo: %d, zborze: %d, produkcja zborza: %d\n", woodRequired, clayRequired, ironRequired, cropRequired, cropUsagePerHour);

        var enough = true;
        var max = Arrays.stream(new int[]{woodRequired, clayRequired, ironRequired}).max().getAsInt();
        if (stockBarWarehouseWrapper < max) {
            enough = false;
        }
        if (wood < woodRequired || clay < clayRequired || iron < ironRequired) {
            enough = false;
        }
        if (stockBarGranaryWrapper < cropRequired || crop < cropRequired) {
            enough = false;
        }
        System.out.println("enough " + enough);
        if (!enough) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            var text = (String) js.executeScript(
                    "var hide = document.getElementsByClassName('hide');\n" +
                            "var timer = hide[0].children[0];\n" +
                            "return timer.getAttribute('value');"
            );
            return toInt(text);
        }
        return 0;
    }

    private static int scanBuildingQueue(WebDriver driver, User user) {
        driver.get(user.getServer());
        System.out.printf("method scanBuildingQueu, currentUrl: %s\n", driver.getCurrentUrl());
        try {
            TimeUnit.SECONDS.sleep(5);

            WebElement boxes = null;

            boxes = driver.findElement(By.className("buildingList"));

            if (boxes == null) {
                System.out.println("buildingList is null");
                return 0;
            } else {
                var value = boxes.findElement(By.className("timer")).getAttribute("value");
                return Integer.valueOf(value);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public static WebDriver login(User user, WebDriver driver) {
        driver.get(user.getServer());
        driver.findElement(By.name("name")).sendKeys(user.getUsername());
        driver.findElement(By.name("password")).sendKeys(user.getPassword());
        driver.findElement(By.id("lowRes")).click();
        driver.findElement(By.id("s1")).click();
        return driver;
    }
}