package pl.maro.travianBot.manager;

import org.openqa.selenium.WebDriver;
import pl.maro.travianBot.domain.User;
import java.util.ArrayList;
import java.util.List;

public class Run {

    static WebDriver driver;

    public static void main(String[] args) {

        User user = new User("aaaaaaaaaaaaaaa", "qwerty", "https://ts2.travian.pl/");
        List<Task> buildingTasks = new ArrayList();


        buildingTasks.add(new Task("https://ts2.travian.pl/build.php?id=16", 1));
        buildingTasks.add(new Task("https://ts2.travian.pl/build.php?id=18", 1));
        buildingTasks.add(new Task("https://ts2.travian.pl/build.php?id=14", 1));
        buildingTasks.add(new Task("https://ts2.travian.pl/build.php?id=17", 1));
        buildingTasks.add(new Task("https://ts2.travian.pl/build.php?id=11", 1));
        buildingTasks.add(new Task("https://ts2.travian.pl/build.php?id=10", 1));

        BuildingService bs = new BuildingService(user, buildingTasks);
        AdventureService as = new AdventureService(user);
//        new Thread(bs).start();
        new Thread(as).start();

    }


}

