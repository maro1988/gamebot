package pl.maro.travianBot.manager;

import lombok.Data;

@Data

public class Task {
    private String buildingUrl;
    private int aimLevel;
    private boolean isDone = false;

    public Task(String buildingUrl, int aimLevel) {
        this.buildingUrl = buildingUrl;
        this.aimLevel = aimLevel;
    }
}
