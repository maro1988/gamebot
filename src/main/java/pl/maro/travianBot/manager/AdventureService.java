package pl.maro.travianBot.manager;

import lombok.Data;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pl.maro.travianBot.domain.Adventure;
import pl.maro.travianBot.domain.Difficulty;
import pl.maro.travianBot.domain.Hero;
import pl.maro.travianBot.domain.User;

import java.lang.reflect.Array;
import java.util.*;

import static pl.maro.travianBot.DataPorcessor.toInt;
import static pl.maro.travianBot.manager.AccountService.login;

@Data
public class AdventureService implements Runnable{

    private User user;

    public AdventureService(User user) {
        this.user = user;
    }

    @Override
    public void run() {
        System.out.println("Start AdventureService");
        WebDriver driver = WebDriverBase.setup(true);
        login(user, driver);
        Hero hero = scanHero(driver);
        ArrayList<Adventure> adventures = scanAdventures(driver);
        adventures.forEach(a ->{
            hero.goAdventure(a);
        });

        System.out.println("End AdventureService");
    }

    private ArrayList<Adventure> scanAdventures(WebDriver driver) {
        driver.get(user.getServer()+"hero.php?t=3");
        var rows = driver
                .findElement(By.id("adventureListForm"))
                .findElement(By.tagName("tbody"))
                .findElements(By.tagName("tr"));
        ArrayList<Adventure> adventures = new ArrayList();
        rows.forEach(tr  -> {
            var a = new Adventure();
            a.setExpiredTime(toInt(tr.findElement(By.className("timer")).getAttribute("value")));
            var difficulty = tr.findElement(By.tagName("img")).getAttribute("class");
            a.setDifficulty(difficulty.equals("adventureDifficulty0") ? 1 : 0);
            adventures.add(a);
        });
        Collections.sort(adventures, Comparator
                .comparing(Adventure::getDifficulty)
                .thenComparing(Adventure::getExpiredTime));

        return adventures;
    }

    private Hero scanHero(WebDriver driver) {
        driver.get(user.getServer()+"hero.php?t=1");
        var health = driver.findElement(By.className("statusWrapper"))
                .findElement(By.className("bar")).getCssValue("width");
        return new Hero(toInt(health));
    }


}
