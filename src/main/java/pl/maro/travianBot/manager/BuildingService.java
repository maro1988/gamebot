package pl.maro.travianBot.manager;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.maro.travianBot.domain.User;
import java.util.List;

import static pl.maro.travianBot.manager.AccountService.build;
import static pl.maro.travianBot.manager.AccountService.login;

@Data
@AllArgsConstructor
public class BuildingService implements Runnable {

    private User user;
    private List<Task> tasks;


    @Override
    public void run() {
        var driver = WebDriverBase.setup(true);
        login(user, driver);
        for (Task t : tasks) {
            build(user, driver, t);
        }
        driver.close();
    }
}
