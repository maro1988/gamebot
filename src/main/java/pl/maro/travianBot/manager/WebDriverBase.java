package pl.maro.travianBot.manager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class WebDriverBase {

    public static WebDriver setup(boolean visible) {
        FirefoxOptions opt = new FirefoxOptions();
        if(!visible) {
            opt.addArguments("--headless");
        }
        return  new FirefoxDriver(opt);
    }
}