package pl.maro.travianBot;

public class DataPorcessor {
    public static int toInt(String text) {
        // text.strip() doesn't work
        // There are chars as 8237 or 8236 in String
        // digits from 48 to 57
        String str = text.codePoints()
                .filter(x -> x >= 48 && x <= 57)
                .collect(StringBuilder::new,
                        StringBuilder::appendCodePoint,
                        StringBuilder::append)
                .toString();
        int result = Integer.parseInt(str);
        return result;
    }
}
